package com.company;

/**
 * Created by lukasz on 23.04.17.
 */
public class PilkarzNozny extends Pilkarz {

    public PilkarzNozny(String imie, String nazwisko, String dataUrodzenia) {
        super(imie, nazwisko, dataUrodzenia);
    }

    public PilkarzNozny(String imie, String nazwisko, String dataUrodzenia, String pozycja, String klub) {
        super(imie, nazwisko, dataUrodzenia, pozycja, klub);
    }

    public PilkarzNozny(String imie, String nazwisko, String dataUrodzenia, String pozycja, String klub, int liczbaGoli) {
        super(imie, nazwisko, dataUrodzenia, pozycja, klub, liczbaGoli);
    }

    @Override
    public void strzelGola() {
        super.strzelGola();
        System.out.println("Pilkarz nozny strzelil gola!");
    }
}
