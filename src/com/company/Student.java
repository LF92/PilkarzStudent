package com.company;

import java.util.ArrayList;

/**
 * Created by lukasz on 23.04.17.
 */
public class Student extends Osoba {

    private int rok;
    private int grupa;
    private int nrIndeksu;
    private ArrayList<Ocena> listaOcen = new ArrayList<>();

    public Student(String imie, String nazwisko, String dataUrodzenia) {
        super(imie, nazwisko, dataUrodzenia);
    }

    public Student(String imie, String nazwisko, String dataUrodzenia, int rok, int grupa, int nrIndeksu) {
        super(imie, nazwisko, dataUrodzenia);
        this.rok = rok;
        this.grupa = grupa;
        this.nrIndeksu = nrIndeksu;
    }

    public int getRok() {
        return rok;
    }

    public void setRok(int rok) {
        this.rok = rok;
    }

    public int getGrupa() {
        return grupa;
    }

    public void setGrupa(int grupa) {
        this.grupa = grupa;
    }

    public int getNrIndeksu() {
        return nrIndeksu;
    }

    public void setNrIndeksu(int nrIndeksu) {
        this.nrIndeksu = nrIndeksu;
    }

    @Override
    public void wypiszInfo() {
        super.wypiszInfo();
        System.out.println(String.format("Rok: %d, grupa: %d, Numer indeksu: %d", rok, grupa, nrIndeksu));
    }

    public void dodajOcene(String przedmiot, String data, double ocena) {
        listaOcen.add(new Ocena(przedmiot,data,ocena));
    }

    public void wypiszOceny() {
        for (Ocena oo : listaOcen) {
            oo.wypiszInfo();
        }
    }

    public void wypiszOceny(String przedmiot) {
        for (Ocena oo : listaOcen) {
            if (oo.getNazwa().equals(przedmiot)) {
                oo.wypiszInfo();
            }
        }
    }

    public void usunOcene(String przedmiot, String data, double ocena) {

        for (int i = 0; i < listaOcen.size(); i++) {
            if(listaOcen.get(i).getNazwa().equals(przedmiot) && listaOcen.get(i).getData().equals(data) && listaOcen
                    .get(i).getWartosc() == ocena) {
                listaOcen.remove(i);
            }

        }
    }

    public void usunOceny() {
        listaOcen.clear();
    }

    public void usunOcene(String przedmiot) {
        for (int i = 0; i < listaOcen.size(); i++) {
            if (listaOcen.get(i).getNazwa().equals(przedmiot)) {
                listaOcen.remove(i);
            }
        }
    }
}
