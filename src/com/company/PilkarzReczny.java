package com.company;

/**
 * Created by lukasz on 23.04.17.
 */
public class PilkarzReczny extends Pilkarz {

    public PilkarzReczny(String imie, String nazwisko, String dataUrodzenia) {
        super(imie, nazwisko, dataUrodzenia);
    }

    public PilkarzReczny(String imie, String nazwisko, String dataUrodzenia, String pozycja, String klub) {
        super(imie, nazwisko, dataUrodzenia, pozycja, klub);
    }

    public PilkarzReczny(String imie, String nazwisko, String dataUrodzenia, String pozycja, String klub, int liczbaGoli) {
        super(imie, nazwisko, dataUrodzenia, pozycja, klub, liczbaGoli);
    }

    @Override
    public void strzelGola() {
        super.strzelGola();
        System.out.println("Pilkarz reczny strzelil!");
    }

}
