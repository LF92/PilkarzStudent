package com.company;

/**
 * Created by lukasz on 23.04.17.
 */
public class Ocena {
    private String nazwa;
    private String data;
    private double wartosc;

    public Ocena(String nazwa, String data, double wartosc) {
        this.nazwa = nazwa;
        this.data = data;
        this.wartosc = wartosc;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public double getWartosc() {
        return wartosc;
    }

    public void setWartosc(double wartosc) {
        this.wartosc = wartosc;
    }

    public void wypiszInfo() {
        System.out.println(String.format("Nazwa przedmiotu: %s, data: %s, ocena: %s", nazwa, data, wartosc));
    }
}
