package com.company;

/**
 * Created by lukasz on 23.04.17.
 */
public class Osoba {

    private String imie;
    private String nazwisko;
    private String dataUrodzenia;

    public Osoba(String imie, String nazwisko, String dataUrodzenia) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getDataUrodzenia() {
        return dataUrodzenia;
    }

    public void setDataUrodzenia(String dataUrodzenia) {
        this.dataUrodzenia = dataUrodzenia;
    }

    public void wypiszInfo() {
        System.out.println(String.format("Imie: %s, Nazwisko: %s, Data urodzenia: %s", imie, nazwisko, dataUrodzenia));
    }
}
