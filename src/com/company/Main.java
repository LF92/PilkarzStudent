package com.company;


public class Main {


    public static void main(String[] args) {
	// write your code here

        Osoba o = new Osoba("Adam", "Miś","20.03.1980");
        Osoba o2 = new Student("Michał","Kot","13.04.1990",2,1,123456);
        Osoba o3 = new Pilkarz("Mateusz","Żbik","10.08.1986","obrońca","FC Czewa");

        o.wypiszInfo();
        o2.wypiszInfo();
        o3.wypiszInfo();

        Student s = new Student("Krzysztof", "Jeż","22.12.1990",2,5,54321);
        Pilkarz p = new Pilkarz("Piotr","Kos","14.09.1984","napastnik","FC Poli");

        s.wypiszInfo();
        p.wypiszInfo();

        ((Pilkarz)o3).strzelGola();
        p.strzelGola();
        p.strzelGola();

        o3.wypiszInfo();
        p.wypiszInfo();

        ((Student)o2).dodajOcene("PO", "20.02.2011",5.0);
        ((Student)o2).dodajOcene("Bazy danych", "13.02.2011",4.0);

        o2.wypiszInfo();

        s.dodajOcene("Bazy danych", "01.05.2011", 5.0);
        s.dodajOcene("AWWW", "11.05.2011", 5.0);
        s.dodajOcene("AWWW", "02.04.2011", 4.5);

        System.out.println("###################################################");
        s.wypiszInfo();
        s.wypiszOceny();

        System.out.println("#####################++##############################");

        //s.usunOcene("AWWW", "02.04.2011", 4.5);
        s.usunOcene("AWWW");
        s.wypiszInfo();
        s.wypiszOceny();

        System.out.println("####################++###############################");
        s.dodajOcene("AWWW", "02.04.2011", 4.5);
        s.wypiszInfo();
        s.wypiszOceny();

        System.out.println("###################################################");
        s.usunOceny();
        s.wypiszInfo();
        s.wypiszOceny();

        Osoba pr = new PilkarzReczny("Jan","Kowal","12.04.2000","atak","FC RTG");
        ((PilkarzReczny)pr).wypiszInfo();
        ((PilkarzReczny)pr).strzelGola();
        ((PilkarzReczny)pr).wypiszInfo();

    }


}
